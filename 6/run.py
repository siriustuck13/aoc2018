#!/usr/bin/env python3
import time

DIM = 370
CENTRAL = 10000
INPUT = 'input'

#DIM = 10
#CENTRAL = 32
#INPUT = 'sample'

def solve1(input):
    i = 65
    grid = [['' for x in range(DIM)] for x in range(DIM)]
    points = []
    for line in input:
        point = parse(line)
        point['point'] = i
        points.append(point)

        try:
            grid[point['y']][point['x']] = point['point']
        except:
            print(point)
            exit()
        i += 1
    
    manhattanize(grid, points)
    return greatestPoint(grid, points)


def solve2(input):
    i = 65
    grid = [['' for x in range(DIM)] for x in range(DIM)]
    points = []
    for line in input:
        point = parse(line)
        point['point'] = i
        points.append(point)

        try:
            grid[point['y']][point['x']] = point['point']
        except:
            print(point)
            exit()
        i += 1

    centerize(grid, points)
    return countCenters(grid)


def parse(line):
    coords = line.split(',')
    x = int(coords[0].strip())
    y = int(coords[1].strip())
    return {'x': x, 'y': y}

def manhattanize(grid, points):
    for y, column in enumerate(grid):
        for x, _ in enumerate(column):
            grid[y][x] = getClosest({'x': x, 'y': y}, points)

def centerize(grid, points):
    for y, column in enumerate(grid):
        for x, _ in enumerate(column):
            grid[y][x] = isCentral({'x': x, 'y': y}, points)

def getClosest(to, points):
    closest = ''
    short = 9999999999
    for p in points:
        dist = distance(to, p)
        if dist < short:
            closest = p['point']
            short = dist
        elif dist == short:
            closest = '.'
    return closest

def isCentral(to, points):
    total = 0
    for p in points:
        total += distance(to, p)
    if total < CENTRAL:
        return '#'
    return '.'

def distance(here, there):
    dx = here['x'] - there['x']
    dy = here['y'] - there['y']
    return abs(dx) + abs(dy)

def greatestPoint(grid, points):
    maxPoint = ''
    maxPoints = 0
    for p in points:
        count = countPoints(grid, p['point'])
        if count > maxPoints:
            maxPoints = count
            maxPoint = p
    return maxPoints


def countPoints(grid, point):
    count = 0
    for y, row in enumerate(grid):
        for x, column in enumerate(row):
            if point == column:
                if x == 0 or y == 0 or x == DIM - 1 or y == DIM - 1: #disqualify edges
                    return 0
                count += 1
    return count

def countCenters(grid):
    count = 0
    for row in grid:
        for char in row:
            if char == '#':
                count += 1
    return count


def printGrid(grid):
    for row in grid:
        #print(''.join([str(a).rjust(3, ' ') for a in row]))
        print(''.join(row))

if __name__ == '__main__':
    with open(INPUT) as input:
        print(solve1(input))
        input.seek(0)
        print(solve2(input))
