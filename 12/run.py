#!/usr/bin/env python3

PAD = "0"*10

def parse(input):
    initLine = input.readline()
    init = ''.join(['1' if x == '#' else '0' for x in initLine[15:]])
    input.readline()
    mutations = []
    for row in input:
        mutation = row.strip()
        case = ("".join(['1' if x == '#' else '0' for x in mutation[:5]]))
        result = '1' if mutation[-1] == '#' else '0'
        mutations.append([int(case,2), result])
    return PAD + init + PAD*20, mutations

def mutateAll(garden, mutations):
    nextGen = ['0' for p in garden]
    for p in range(2, len(garden) - 2):
        case = garden[p-2:p+3]
        for m in mutations:
            mask = m[0]
            if int(case, 2) == mask:
                nextGen[p] = m[1]
                break
    if garden[-5:] != '00000' or garden[:5] != '00000':
        print("NEED MORE PADDING")
        exit()
    return ''.join(nextGen)

def calculate(garden, add=0):
    score = 0
    for i in range(0, len(garden)):
        if garden[i] == '1':
            score += (i - len(PAD) + add)
    return score
        
if __name__ == '__main__':
    print('Day 12')
    with open('sample') as input:
        garden, mutations = parse(input)
        assert int(garden.strip('0'), 2) == int('1001010011000000111000111', 2)
        for gen in range(20):
            garden = mutateAll(garden, mutations)
            test = garden.strip('0')
            if gen == 0:
                assert int(test, 2) == int('1000100001000001001001001', 2)
            if gen == 1:
                assert int(test, 2) == int('11001100011000010010010011', 2)
            if gen == 4:
                assert int(test, 2) == int('10001100010100100100010001', 2)
            if gen == 9:
                assert int(test, 2) == int('1010010001011000011001100110011', 2)
        assert int(garden.strip('0'), 2) == int('1000011000011111000111111100001010011', 2)
        assert calculate(garden) == 325

    with open('input') as input:
        garden, mutations = parse(input)
        for gen in range(20):
            garden = mutateAll(garden, mutations)
        print('Gen 20:', calculate(garden))

    PATTERN = '100101100010110000100001011000100000101100010110001011000101100010010110001011000101100010110001011000101100010110001011000101100010110001011'
    # --- This is how we found the pattern
    with open('input') as input:
        garden, mutations = parse(input)
        #print('--'+' '*len(PAD) , '         |'*10)
        #print('--'+' '*len(PAD) + '0123456789'*10)
        last = None 
        for gen in range(1, 50000000000):
            garden = mutateAll(garden, mutations)
            if gen > 122:
                #print(gen, garden)
                assert calculate(garden) == calculate(garden.strip('0'), gen-41+len(PAD))
                assert garden.strip('0') == PATTERN
            if gen > 150:
                break
    gen = 50000000000
    print('Gen 50 billion:', calculate(PATTERN, gen-41+len(PAD)))

