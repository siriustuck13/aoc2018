#!/usr/bin/env python3
import os
import time

SAMPLE = False
if SAMPLE:
    INPUT = 'sample'
    POSLEN = 2
    VELLEN = 2
else:
    INPUT = 'input'
    POSLEN = 6
    VELLEN = 2

class Point():
    def __init__(self, x, y, dx, dy):
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy
    
    def __repr__(self):
        return str(self.x) + ' ' + \
            str(self.y) + ' ' + \
            str(self.dx) + ' ' + \
            str(self.dy)

    def move(self):
        self.x += self.dx
        self.y += self.dy

class Grid():
    def __init__(self, points):
        self.points = points
        self.moves = 0
    
    def __repr__(self):
        grid, xMod, yMod = self.makeGrid()
        out = 'moves: ' + str(self.moves) + '\n'
        if grid is None:
            return out
        for p in points:
            grid[p.y - yMod][p.x - xMod] = '#'
        for row in grid:
            out += (''.join(row) + '\n')
        return out
    
    def move(self):
        for p in self.points:
            p.move()
        self.moves += 1
    
    def makeGrid(self):
        minX = None
        minY = None
        maxX = None
        maxY = None
        for p in self.points:
            if minX is None or p.x < minX:
                minX = p.x
            if maxX is None or p.x > maxX:
                maxX = p.x
            if minY is None or p.y < minY:
                minY = p.y
            if maxY is None or p.y > maxY:
                maxY = p.y
        width = maxX - minX + 1
        height = maxY - minY + 1
        if width > 100 or height > 100:
            return None, 0, 0
        return [['.' for x in range(width)] for x in range(height)], minX, minY
        
def parsePoint(line):
    i = 0
    i += len('position=<')
    x = int(line[i:i+POSLEN])
    i += POSLEN
    i += len(', ')
    y = int(line[i:i+POSLEN])
    i += POSLEN
    i += len('> velocity=<')
    dx = int(line[i:i+VELLEN])
    i += VELLEN
    i += len(', ')
    dy = int(line[i:i+VELLEN])

    return Point(x, y, dx, dy)

def parseInput(input):
    points = []
    for line in input:
        points.append(parsePoint(line))
    return points

def animate(grid):
    while True:
        if grid.moves > 10010:
            os.system('clear')
            print(grid)
            exit()
        grid.move()

def solve1(input):
    return None

def solve2(input):
    return None

if __name__ == '__main__':
    with open(INPUT) as input:
        points = parseInput(input)
        grid = Grid(points)

        animate(grid)
