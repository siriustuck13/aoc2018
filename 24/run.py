#!/usr/bin/env python3
import re
import time
import curses
import sys
import math

IMMUNE = 'immune'
INFECT = 'infection'

def ints(s):
    return list(map(int, re.findall(r"-?\d+", s)))

class UnitGroup():
    def __init__(self, id=0, groupName='', units=0, hp=0, weakness=[], immune=[], atkType='', atkDmg=0, initiative=0):
        self.id = id
        self.groupName = groupName
        self.units = units # 0
        self.hp = hp # 0
        self.weakness = weakness # []
        self.immune = immune # []
        self.atkType = atkType # ''
        self.atkDmg = atkDmg # 0
        self.initiative = initiative # 0
        self.target = None
    
    def __repr__(self):
        return self.groupName + ' ' + str(self.id) + ' - Troops: ' + str(self.units) + ' Target: ' + ('' if self.target is None else str(self.target.id))
    
    def effectivePower(self):
        return self.units * self.atkDmg
    
    def estimateDamage(self, atkType, atkDmg):
        if atkType in self.immune:
            return 0
        elif atkType in self.weakness:
            return atkDmg * 2
        return atkDmg
    
    def pickBest(self, opponents):
        self.target = None
        bestDmg = 0
        best = None
        for o in opponents:
            if o.groupName == self.groupName:
                continue
            dmg = o.estimateDamage(self.atkType, self.effectivePower())
            if dmg == 0:
                continue
            if dmg > bestDmg:
                bestDmg = dmg
                best = o
            elif dmg == bestDmg:
                ep = o.effectivePower()
                bestEp = best.effectivePower()
                if ep > bestEp:
                    best = o
                elif ep == bestEp:
                    if o.initiative > best.initiative:
                        best = o
        self.target = best
        return best
    
    def attack(self):
        if self.target is None:
            return
        dmg = self.target.estimateDamage(self.atkType, self.effectivePower())
        killed = math.floor(dmg/self.target.hp)
        self.target.units -= killed
        if self.target.units < 0:
            killed += self.target.units
        #print(self.groupName, self.id, 'killed', killed, self.target.groupName, self.target.id)
        return self.target.isDead()

    def isDead(self):
        return self.units <= 0

def fight(units):
    # target selection
    allUnits = units[IMMUNE] + units[INFECT]
    targetUnits = targetOrder(allUnits) 
    fightUnits = fightOrder(allUnits)
    availableTargets = units[IMMUNE] + units[INFECT]
    for u in targetUnits:
        target = u.pickBest(availableTargets)
        if target is not None:
            availableTargets.remove(target)
    
    i = 0
    while i < len(fightUnits):
        u = fightUnits[i]
        killed = u.attack()
        if killed:
            fightUnits.remove(u.target)
            i = fightUnits.index(u)
        i += 1
    
    units[IMMUNE] = filterAlive(units[IMMUNE])
    units[INFECT] = filterAlive(units[INFECT])
    if len(units[IMMUNE]) == 0:
        return sum([u.units for u in units[INFECT]]), False
    if len(units[INFECT]) == 0:
        return sum([u.units for u in units[IMMUNE]]), True
    return None, False

def targetOrder(units):
    units = fightOrder(units)
    return sorted(units, key=lambda u: u.effectivePower(), reverse=True)

def fightOrder(units):
    return sorted(units, key=lambda u: u.initiative, reverse=True)

def opponent(unit):
    return IMMUNE if unit.groupName == INFECT else INFECT

def filterAlive(units):
    return list(filter(lambda u: not u.isDead(), units))
    
def parseLine(line, id, groupName, boost):
    numbers = ints(line.strip())
    units = numbers[0]
    hp = numbers[1]
    atkDmg = numbers[2] + boost
    initiative = numbers[3]

    atkType = re.search(r"([a-z]*) damage", line)
    if atkType is not None:
        atkType = atkType.group(1)

    immune = re.search(r"immune to ([a-z, ]*)", line)
    if immune is not None:
        immune = immune.group(1).split(', ')
    else:
        immune = []
    weakness = re.search(r"weak to ([a-z, ]*)", line)
    if weakness is not None:
        weakness = weakness.group(1).split(', ')
    else:
        weakness = []
    return UnitGroup(
        id, groupName,
        units, hp, weakness, immune,
        atkType, atkDmg, initiative
    )

def printUnits(allUnits):
    print('=IMMUNE=')
    for u in allUnits[IMMUNE]:
        print(u)
    print('=INFECT=')
    for u in allUnits[INFECT]:
        print(u)
    print('=ATTACK=')


def solve(file, boost, debug=False):
    units = {
        'immune': [],
        'infection': []
    }
    group = None
    id = 1
    for line in file:
        line = line.strip()
        if line == 'Immune System:':
            group = IMMUNE
            id = 1
            continue
        elif line == 'Infection:':
            group = INFECT
            id = 1
            continue
        elif line == '':
            continue
        unit = parseLine(line, id, group, (boost if group == IMMUNE else 0))
        units[group].append(unit)
        id += 1
    
    while True:
        if debug:
            printUnits(units)
        result, won = fight(units)
        if result is not None:
            return result, won

if __name__ == '__main__':
    with open('sample') as file:
        print('S1.', solve(file, 0, False))
    
    with open('input') as file:
        print('Q1.', solve(file, 0, False))
    
    with open('sample') as file:
        best = 0 
        min = 0
        max = 99999999
        while True:
            boost = math.ceil((min+max)/2)
            if boost == max:
                print('S2.', best)
                break
            result, win = solve(file, boost, False)
            if win:
                best = result
                max = boost
            if not win:
                min = boost
            file.seek(0)
                
    with open('input') as file:
        best = 0 
        min = 49
        max = 49
        while True:
            boost = math.ceil((min+max)/2)
            result, win = solve(file, boost, False)
            if win:
                best = result
                max = boost
            if not win:
                min = boost
            if boost == max:
                print('Q2.', best)
                break
            file.seek(0)
                
    
    