#!/usr/bin/env python3
import re

def ints(s):
    return list(map(int, re.findall(r"-?\d+", s)))

def manhattan(n1, n2):
    dist = 0
    dist += abs(n1[0] - n2[0])
    dist += abs(n1[1] - n2[1])
    dist += abs(n1[2] - n2[2])
    return dist

def parseNanobots(file):
    # x,y,z,r
    nanobots = []
    bestR = 0
    best = None
    for line in file:
        coords = ints(line)
        nanobots.append((coords[0], coords[1], coords[2], coords[3]))
        if coords[3] > bestR:
            best = nanobots[-1]
            bestR = coords[3]

    return nanobots, best

def cubedNodes(nanobots):
    cubes=[]
    for n in nanobots:
        x = n[0]
        y = n[1]
        z = n[2]
        r = n[3]
        cube = (x-r, y-r, z-r, z+r, y+r, z+r)
        cubes.append(cube)

def intersect(min1, max1, min2, max2):
    return (min1 <= min2 and max1 >= min2) or \
        (min2 <= min1 and max2 >= min1)

def cubesIntersect(c1, c2):
    return intersect(c1[0], c1[3], c2[0], c2[3]) or \
        intersect(c1[1], c1[4], c2[1], c2[4]) or \
        intersect(c1[2], c1[5], c2[2], c2[5])

def trythis(cubes, xMin, yMin, zMin, xMax, yMax, zMax):
    x = xMin
    y = yMin
    z = zMin
    dx = xMax - xMin
    dy = yMax - yMin
    dz = zMax - zMin

    bestScore = 0
    best = None
    while x < xMax:
        while y < yMax:
            while z < zMax:
                c2 = (x, y, z, x+dx, y+dy, z+dz)
                count = 0
                for c in cubes:
                    count += 1 if cubesIntersect(c, c2) else 0
                if count > bestScore:


    for cube in cubes:
        tl += 1 if cubesIntersect((xMin, yMin, zMin, xMin+dx, yMin+dy, zMin+dz), cube) else 0
        tr += 1 if cubesIntersect((xMin+dx, yMin, zMin, xMax, yMax, zMax), cube) else 0



def solve(nanobots, best):
    inRange = 0
    for n in nanobots:
        dist = manhattan(best, n)
        if manhattan(best, n) <= best[3]:
            inRange += 1
    return inRange

def solve2(nanobots):
    grid = {}
    for n in nanobots:
        grid = markInRange(grid, n)
    
    bestCount = 0
    best = [] 
    for _, coords in enumerate(grid):
        score = grid[coords]
        if score > bestCount:
            bestCount = score
            best = [coords]
        if score == bestCount:
            best.append(coords)
    return best, bestCount

def markInRange(grid, n):
    print(n)
    x = n[0]
    y = n[1]
    z = n[2]
    r = n[3]
    rm=r+1
    for xm in range(x, x+rm):
        for ym in range(y, y-abs(xm-x)+rm):
            for zm in range(z, z-(ym-y)-(xm-x)+rm):
                for nm in mirrors(xm, ym, zm, n):
                    if nm not in grid:
                        grid[nm] = 0
                    grid[nm] += 1
    return grid

def mirrors(x, y, z, over):
    dx = 2*(x-over[0])
    dy = 2*(y-over[1])
    dz = 2*(z-over[2])
    values = [
        (   x,    y,    z), # point
        (x-dx,    y,    z), # mirror horizontal
        (   x, y-dy,    z), # mirror vertical
        (   x,    y, z-dz), # mirror Z
        (x-dx, y-dy,    z), # mirror horizontal/vertical
        (x-dx,    y, z-dz), # mirror horizontal/Z
        (   x, y-dy, z-dz), # mirror vertical/Z
        (x-dx, y-dy, z-dz), # mirror all
    ]
    mirr = []
    for v in values:
        if v not in mirr:
            mirr.append(v)
    return mirr

if __name__ == '__main__':
    with open('sample') as file:
        nanobots, best = parseNanobots(file)
        score = solve(nanobots, best)
        assert score == 7
    
    with open('sample2') as file:
        nanobots, best = parseNanobots(file)
        print(solve2(nanobots))

    with open('input') as file:
        nanobots, best = parseNanobots(file)
        print(solve(nanobots, best))
        print(solve2(nanobots))