#!/usr/bin/env python3

import time
import curses

NORTH = 'N'
SOUTH = 'S'
WEST = 'W'
EAST = 'E'

class Room():
    def __init__(self, x=0, y=0):
        self.n = None
        self.e = None
        self.s = None
        self.w = None
        self.x = x
        self.y = y
    
    def calculateScore(self):
        self.score = None
        for r in [self.n, self.e, self.s, self.w]:
            if r is not None:
                if self.score is None:
                    self.score = r.score+1
                elif r.score < self.score:
                    self.score = r.score+1
        if self.score is None:
            self.score = 0
    
    def nextPos(self, direction):
        if direction == NORTH:
            return self.x, self.y-1
        if direction == EAST:
            return self.x+1, self.y
        if direction == SOUTH:
            return self.x, self.y+1
        if direction == WEST:
            return self.x-1, self.y
        print(direction)
    
    def print(self, pad, xPad, yPad, style=0):
        if pad is None:
            return
        xPad = -120
        yPad = -120
        x = self.x*2
        y = self.y*2
        
        top = ('█' + (' ' if self.n is not None else '█')  + '█')
        mid = ((' ' if self.w is not None else '█') + ' ' + (' ' if self.e is not None else '█'))
        bot = ('█' + (' ' if self.s is not None else '█') + '█')
        pad.addstr(y-1-yPad, x-1-xPad, top, style)
        pad.addstr(  y-yPad, x-1-xPad, mid, style)
        pad.addstr(y+1-yPad, x-1-xPad, bot, style)

class Facility():
    def __init__(self):
        self.map = {(0, 0): Room()}
        self.xMin = 0
        self.xMax = 0
        self.yMin = 0
        self.yMax = 0
        self.highScore = 0
    
    def move(self, room, direction):
        x, y = room.nextPos(direction)
        if (x, y) not in self.map:
            self.map[(x, y)] = Room(x, y)
        newRoom = self.map[(x, y)]
        setattr(room, direction.lower(), newRoom)
        setattr(newRoom, inverse(direction).lower(), room)
        newRoom.calculateScore()
        if newRoom.score > self.highScore:
            self.highScore = newRoom.score
        self.grow(x, y)
        return self.map[(x, y)]
    
    def grow(self, x, y):
        if x < self.xMin:
            self.xMin = x
        elif x > self.xMax:
            self.xMax = x
        if y < self.yMin:
            self.yMin = y
        elif y > self.yMax:
            self.yMax = y

    def print(self, pad=None):
        for pos in self.map:
            self.map[pos].print(pad, self.xMin-1, self.yMin-1)
        if pad is None:
            return
        pad.addstr(0, 0, 'High Score:' + str(self.highScore))
        pad.refresh(0, 0, 0, 0, 250, 300)
        #pad.refresh(0, 0, 0, 0, 60, 200)
        #time.sleep(0.01)
    
def inverse(direction):
    if direction == NORTH:
        return SOUTH
    if direction == SOUTH:
        return NORTH
    if direction == EAST:
        return WEST
    if direction == WEST:
        return EAST

def parse(input):
    options = []
    option = [] 
    i = 0
    while i < len(input):
        char = input[i]
        i += 1
        if char in '^':
            continue
        elif char in '(':
            nested, count = parse(input[i:])
            i += count
            option.append(nested)
        elif char in ')$':
            options.append(list(option))
            return options, i
        elif char in '|':
            options.append(list(option))
            option = []
        else:
            option.append(char)

def move(facility, room, instructions, pad=None):
    for i in instructions:
        if len(i) > 1:
            choose(facility, room, i, pad)
        else:
            room = facility.move(room, i)
        facility.print(pad)

def choose(facility, room, options, pad=None):
    for o in options:
        move(facility, room, o, pad)

def solve(regex, pad=None):
    f = Facility()
    room = f.map[(0, 0)]
    room.calculateScore()
    options, _ = parse(regex)
    choose(f, room, options, pad)
    far = 0
    for room in f.map:
        if f.map[room].score >= 1000:
            far+=1
    return f.highScore, far


def main(stdscr):
    pad = curses.newpad(2000, 2000)

    with open('simple') as file:
        regex = file.readline().strip()
        score, count = solve(regex, pad)
        pad.getch()
        pad.clear()
        assert score == 3
        assert count == 0

    with open('sample') as file:
        regex = file.readline().strip()
        score, count = solve(regex, pad)
        pad.getch()
        pad.clear()
        assert score == 18
        assert count == 0

    with open('input') as file:
        regex = file.readline().strip()
        score = solve(regex, pad)
        pad.getch()
        pad.clear()

if __name__ == '__main__':
    with open('input') as file:
        regex = file.readline().strip()
        score, count = solve(regex)
        print('Q1. ', score)
        print('Q2. ', count)
        input()

    curses.wrapper(main)