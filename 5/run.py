#!/usr/bin/env python3

ORD_DIFF = 32

def strip(input):
    hit = False
    i = 0
    out = ''
    while i < len(input)-1:
        a = input[i]
        b = input[i+1]
        if abs(ord(a) - ord(b)) == ORD_DIFF:
            i = i + 2
            hit = True
        else:
            out = out + a
            if i == len(input) - 2:
                out = out + b
            i = i + 1
    return hit, out

def solve1(input):
    for line in input:
        hit = True
        while hit:
            hit, line = strip(line)
        return len(line)
    return None


def solve2(input):
    for line in input:
        bestchar = None
        bestcount = len(line)
        for char in 'abcdefghijklmnopqrstuvwxyz':
            hit = 1
            cp = line
            cp = cp.replace(char, '')
            cp = cp.replace(chr(ord(char) - ORD_DIFF), '')
            while hit > 0:
                hit, cp = strip(cp)
            
            if len(cp) < bestcount:
                bestcount = len(cp)
                bestchar = char
    return bestcount, bestchar


def parse(line):
    return None


if __name__ == '__main__':
    with open('input') as input:
        print(solve1(input))
        input.seek(0)
        print(solve2(input))
