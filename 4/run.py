#!/usr/bin/env python3

import pendulum

WAKE = 'wakes up'
SLEEP = 'falls asleep'


def sortTimes(x):
    return x['time']


def solve1(input):
    timesheet = []
    for line in input:
        timesheet.append(parse(line))
    timesheet = sorted(timesheet, key=sortTimes)

    sleep = {}
    schedule = {}

    sleepingSince = None
    activeGuard = ""
    for cmd in timesheet:
        command = cmd['command']
        time = cmd['time']
        if command[0] == '#':
            # guard shift
            activeGuard = command
            sleepingSince = None
        elif command == SLEEP:
            sleepingSince = time
        elif command == WAKE:
            duration = time - sleepingSince
            sched = {
                'start': sleepingSince,
                'duration': duration
            }
            if activeGuard in sleep:
                sleep[activeGuard] = sleep[activeGuard] + duration
                schedule[activeGuard].append(sched)
            else:
                sleep[activeGuard] = (time - sleepingSince)
                schedule[activeGuard] = [sched]

    lazy = None
    for guard in sleep:
        if lazy is None:
            lazy = guard
        elif sleep[guard] > sleep[lazy]:
            lazy = guard

    maxtime = getMaxTime(schedule[lazy])

    return {
        'guard': lazy,
        'time': maxtime['max']
    }


def getMaxTime(schedule):
    times = {}
    for entry in schedule:
        now = entry['start']
        while now < entry['start'] + entry['duration']:
            key = str(now.hour) + str(now.minute)
            if key in times:
                times[key] = times[key] + 1
            else:
                times[key] = 1
            now = now.add(minutes=1)
    maxtime = None
    for t in times:
        if maxtime is None:
            maxtime = t
        elif times[maxtime] < times[t]:
            maxtime = t
    return {
        'max': maxtime,
        'freq': times[maxtime]
    }


def solve2(input):
    timesheet = []
    for line in input:
        timesheet.append(parse(line))
    timesheet = sorted(timesheet, key=sortTimes)

    sleep = {}
    schedule = {}

    sleepingSince = None
    activeGuard = ""
    for cmd in timesheet:
        command = cmd['command']
        time = cmd['time']
        if command[0] == '#':
            # guard shift
            activeGuard = command
            sleepingSince = None
        elif command == SLEEP:
            sleepingSince = time
        elif command == WAKE:
            duration = time - sleepingSince
            sched = {
                'start': sleepingSince,
                'duration': duration
            }
            if activeGuard in sleep:
                sleep[activeGuard] = sleep[activeGuard] + duration
                schedule[activeGuard].append(sched)
            else:
                sleep[activeGuard] = (time - sleepingSince)
                schedule[activeGuard] = [sched]

    worstGuard = None
    worstTime = None
    for guard in sleep:
        maxtime = getMaxTime(schedule[guard])
        if worstGuard is None:
            worstGuard = guard
            worstTime = maxtime
        elif worstTime['freq'] < maxtime['freq']:
            worstGuard = guard
            worstTime = maxtime

    return {
        'guard': worstGuard,
        'time': worstTime
    }


def parse(line):
    bits = line.split(']')
    ts = bits[0][1:]
    time = pendulum.parse(ts)

    command = bits[1][1:].strip()
    if command not in [WAKE, SLEEP]:
        more = command.split(' ')
        command = more[1]
    return {
        "time": time,
        "command": command
    }


if __name__ == '__main__':
    with open('input') as input:
        print(solve1(input))
        input.seek(0)
        print(solve2(input))
