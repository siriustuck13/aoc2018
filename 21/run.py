#!/usr/bin/env python3

from cpu import CPU

def solve(puzzle, register, ip, debug=False):
    instructions = []
    for line in puzzle:
        opr = line[0:4]
        instr = [int(i) for i in line[5:].split(' ')]
        instructions.append([opr] + instr)

    first = None
    last = None
    answers = {}
    i = 0
    cpu = CPU(register)
    while i < len(instructions):
        if i == 4:
            return None
        cpu.register[ip] = i
        cpu.execute(instructions[i])
        if i == 28:
            r = cpu.register[3]
            if first is None:
                first = r
            if r not in answers:
                answers[r] = r
                last = r
            else:
                return first, last
        i = cpu.register[ip]
        i += 1
    return cpu.register[0]

if __name__ == '__main__':
    with open('input') as file:
        #ip 4
        q1, q2 = solve(file, [-1,0,0,0,0,0], 4)
        print('Q1.', q1)
        print('Q2.', q2)
