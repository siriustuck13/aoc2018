#!/usr/bin/env python3
import json

METHODS = [
    "addr",
    "addi",
    "mulr",
    "muli",
    "banr",
    "bani",
    "borr",
    "bori",
    "setr",
    "seti",
    "gtir",
    "gtri",
    "gtrr",
    "eqir",
    "eqri",
    "eqrr",
]

class CPU():
    def __init__(self, register):
        self.register = register
    
    def __repr__(self):
        return ','.join([str(r).rjust(14) for r in self.register])

    def execute(self, instr):
        getattr(self, instr[0])(instr[1], instr[2], instr[3])
    
    def addr(self, a, b, c):
        """
        (add register) stores into register C the result of adding register A and register B.
        """
        self.register[c] = self.register[a] + self.register[b]
    def addi(self, a, b, c):
        """
        (add immediate) stores into register C the result of adding register A and value B.
        """
        self.register[c] = self.register[a] + b
    def mulr(self, a, b, c):
        """
        (multiply register) stores into register C the result of multiplying register A and register B.
        """
        self.register[c] = self.register[a] * self.register[b]
    def muli(self, a, b, c):
        """
        (multiply immediate) stores into register C the result of multiplying register A and value B.
        """
        self.register[c] = self.register[a] * b
    def banr(self, a, b, c):
        """
        (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
        """
        self.register[c] = self.register[a] & self.register[b]
    def bani(self, a, b, c):
        """
        (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
        """
        self.register[c] = self.register[a] & b
    def borr(self, a, b, c):
        """
        (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
        """
        self.register[c] = self.register[a] | self.register[b]
    def bori(self, a, b, c):
        """
        (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
        """
        self.register[c] = self.register[a] | b
    def setr(self, a, b, c):
        """
        (set register) copies the contents of register A into register C. (Input B is ignored.)
        """
        self.register[c] = self.register[a]
    def seti(self, a, b, c):
        """
        (set immediate) stores value A into register C. (Input B is ignored.)
        """
        self.register[c] = a
    def gtir(self, a, b, c):
        """
        (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
        """
        self.register[c] = 1 if a > self.register[b] else 0
    def gtri(self, a, b, c):
        """
        (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
        """
        self.register[c] = 1 if self.register[a] > b else 0
    def gtrr(self, a, b, c):
        """
        (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
        """
        self.register[c] = 1 if self.register[a] > self.register[b] else 0
    def eqir(self, a, b, c):
        """
        (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0.
        """
        self.register[c] = 1 if a == self.register[b] else 0
    def eqri(self, a, b, c):
        """
        (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
        """
        self.register[c] = 1 if self.register[a] == b else 0
    def eqrr(self, a, b, c):
        """
        (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
        """
        self.register[c] = 1 if self.register[a] == self.register[b] else 0
