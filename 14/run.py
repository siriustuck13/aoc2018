#!/usr/bin/env python3
import os
import time

from linkedlist import DoublyLinkedList, DListNode

def moveElf(list, elf):
    for _ in range(int(elf.data) + 1):
        if elf.next == None:
            elf = list.head
        else:
            elf = elf.next
    return elf

def solve1(input):
    recipes = DoublyLinkedList()
    recipes.append('3')
    recipes.append('7')

    elf1 = recipes.head
    elf2 = recipes.head.next

    i = 0
    while i < input+10:
        newRecipe = str(int(elf1.data) + int(elf2.data))
        for d in newRecipe:
            recipes.append(d)
            i += 1
        elf1 = moveElf(recipes, elf1)
        elf2 = moveElf(recipes, elf2)
        
    return str(recipes)[input:input+10]

def solve2(input):
    recipes = DoublyLinkedList()
    recipes.append('3')
    recipes.append('7')

    elf1 = recipes.head
    elf2 = recipes.head.next

    i = 0
    while True:
        newRecipe = str(int(elf1.data) + int(elf2.data))
        for d in newRecipe:
            recipes.append(d)
            i += 1
        elf1 = moveElf(recipes, elf1)
        elf2 = moveElf(recipes, elf2)

        # Check for done
        end = recipes.getLastN(len(input)+1)
        if input in end: 
            modify = -1 if end[:-1] == input else 0
            return i + 2 - len(input) + modify

        
if __name__ == '__main__':
    print('Day 13')
    # Samples
    assert solve1(9) == '5158916779'
    assert solve1(5) == '0124515891'
    assert solve1(18) == '9251071085'
    assert solve1(2018) == '5941429882'

    assert solve2('1') == 2
    assert solve2('51589') == 9
    assert solve2('01245') == 5
    assert solve2('92510') == 18
    assert solve2('59414') == 2018

    # Puzzle
    recipes = 209231
    print("Q1.", solve1(209231))
    print("Q2.", solve2('209231'))