#!/usr/bin/env python3


def solve1(input):
    fabric = [['0' for x in range(1000)] for x in range(1000)]
    for line in input:
        vars = parse(line)
        fabric = claim(fabric, **vars)
    return countDoubles(fabric)


def solve2(input):
    fabric = [['0' for x in range(1000)] for x in range(1000)]
    should = {'0': None}
    for line in input:
        vars = parse(line)
        should[vars['id']] = vars['width'] * vars['height']
        fabric = claim(fabric, **vars)
    values = countById(fabric)
    for id in should:
        if id in values and should[id] == values[id]:
            return id


def parse(line):
    line = line.strip()
    bits = line.split(' ')
    id = bits[0][1:]
    coords = bits[2].split(',')
    dim = bits[3].split('x')
    return {
        'id': id,
        'x': int(coords[0]),
        'y': int(coords[1][:-1]),
        'width': int(dim[0]),
        'height': int(dim[1]),
    }


def claim(fabric, id, x, y, width, height):
    i = x
    while i < x + width:
        j = y
        while j < y + height:
            if fabric[i][j] == '0':
                fabric[i][j] = id
            else:
                fabric[i][j] = 'X'
            j = j + 1
        i = i + 1
    return fabric


def countDoubles(fabric):
    count = 0
    for row in fabric:
        for val in row:
            if val == 'X':
                count = count + 1
    return count


def countById(fabric):
    values = {}
    for row in fabric:
        for val in row:
            if val == 'X':
                continue
            elif str(val) in values:
                values[val] = values[val] + 1
            else:
                values[val] = 1
    return values


def printFabric(fabric):
    for row in fabric:
        strs = [str(a).rjust(3, ' ') for a in row]
        print(' '.join(strs))


if __name__ == '__main__':
    with open('input') as input:
        print(solve1(input))
        input.seek(0)
        print(solve2(input))
