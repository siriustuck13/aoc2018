#!/usr/bin/env python3
import os
import time

CELLS = 300
AREA = 3

def makeGrid(serial):
    return [[calculateScore(serial, x, y) for x in range(CELLS)] for y in range(CELLS)]

def calculateScore(serial, x, y):
    x += 1
    y += 1
    rackID = x + 10
    power = rackID * y
    power += serial
    power *= rackID
    power = int(str(power)[-3])
    power -= 5
    return power

def areaScore(grid, x, y, dim=3):
    return sum([sum(y[x:x+dim]) for y in grid[y:y+dim]])
    
def bestArea(grid, dim=3):
    bestScore = -99999999
    bestX = None
    bestY = None
    for x in range(CELLS-dim):
        for y in range(CELLS-dim):
            score = areaScore(grid, x, y, dim)
            if score > bestScore:
                bestScore = score
                bestX = x+1
                bestY = y+1
    return bestScore, bestX, bestY

def bestPossibleArea(grid):
    bestScore = -99999
    bestX = 0
    bestY = 0
    bestDim = 0
    for dim in range(CELLS):
    #for dim in reversed(range(CELLS)):
        if dim == 0:
            continue
        if dim > 50:
            break
        score, x, y = bestArea(grid, dim)
        if score > bestScore:
            bestScore = score
            bestX = x
            bestY = y
            bestDim = dim
    return bestScore, bestX, bestY, bestDim

if __name__ == '__main__':
    grid = makeGrid(57)
    assert grid[79-1][122-1] == -5
    grid = makeGrid(39)
    assert grid[196-1][217-1] == 0
    grid = makeGrid(71)
    assert grid[153-1][101-1] == 4
    grid = makeGrid(18)
    assert areaScore(grid, 33-1, 45-1) == 29
    assert areaScore(grid, 90-1, 269-1, 16) == 113
    score, x, y = bestArea(grid, 16)
    assert score == 113
    assert x == 90
    assert y == 269
    grid = makeGrid(42)
    assert areaScore(grid, 21-1, 61-1) == 30
    assert areaScore(grid, 232-1, 251-1, 12)

    myGrid = makeGrid(9445)
    print(','.join([str(x) for x in bestArea(myGrid)][1:]))
    print(','.join([str(x) for x in bestPossibleArea(myGrid)][1:]))

