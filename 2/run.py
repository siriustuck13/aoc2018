#!/usr/bin/env python3


def solve1(input):
    twos = []
    threes = []
    for line in input:
        is2 = False
        is3 = False
        for char in line[:-1]:
            c = count(char, line)
            if not is2 and c == 2:
                twos.append(line)
                is2 = True
            elif not is3 and c == 3:
                threes.append(line)
                is3 = True
            elif is2 and is3:
                break
    return len(twos) * len(threes)


def solve2(input):
    opts = list(input)
    input.seek(0)
    for line in input:
        for opt in opts:
            if countDiff(line.strip(), opt.strip()) == 1:
                return ''.join(mergeDiff(line.strip(), opt.strip()))


def count(letter, str):
    return str.count(letter)


def countDiff(str1, str2):
    return sum(1 for a, b in zip(str1, str2) if a != b)


def mergeDiff(str1, str2):
    return [a for a, b in zip(str1, str2) if a == b]


if __name__ == '__main__':
    with open('input') as input:
        print(solve1(input))
        input.seek(0)
        print(solve2(input))
