#!/usr/bin/env python3
import time

SAMPLE = True
if SAMPLE:
    INPUT = 'sample'
    MODIFIER = 0
    ELFCOUNT = 2
    FOUND = 'ABCDEF'
else:
    INPUT = 'input'
    MODIFIER = 60
    ELFCOUNT = 5
    FOUND = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def buildTree(input):
    nodes = {}
    prereqs = {}
    for line in input:
        bits = line.split(' ')
        p = bits[1]
        c = bits[7]
        if p not in nodes:
            nodes[p] = []
        if c not in prereqs:
            prereqs[c] = []
        nodes[p].append(c)
        prereqs[c].append(p)

    return nodes, prereqs


def findFirst(tree):
    found = FOUND
    for k, v in tree.items():
        for n in v:
            found = found.replace(n, '')
    return list(found)


def solve1(tree, prereqs):
    options = findFirst(tree)
    order = ''
    while True:
        valid = []
        for o in options:
            if o not in prereqs or satisfied(order, prereqs[o]):
                valid.append(o)
        node = valid[0]
        order += node
        options = list(''.join(options).replace(node, ''))
        if node not in tree:
            return order
        options += tree[node]
        options.sort()


def satisfied(done, prereqs):
    for r in prereqs:
        if r not in done:
            return False
    return True


def solve2(tree, prereqs):
    elves = [None for elf in range(ELFCOUNT)]
    second = 0

    options = findFirst(tree)
    done = ''
    while True:
        if SAMPLE:
            print(
                second,
                ['.' if elf is None else elf['node'] for elf in elves],
                list(set(options)), done
            )
        valid = []
        for idx, elf in enumerate(elves):
            if elf is not None and second >= elf['until']:
                if elf['node'] not in tree:
                    return second
                options += tree[elf['node']]
                done += elf['node']
                elves[idx] = None
        for o in options:
            if o not in prereqs or satisfied(done, prereqs[o]):
                valid.append(o)
        valid = list(set(valid))
        valid.sort()
        for idx, elf in enumerate(elves):
            if len(valid) > 0 and elf == None:
                node = valid[0]
                elves[idx] = {
                    'node': node,
                    'until': second + (ord(node) - 64 + MODIFIER)
                }
                valid = valid[1:]
                options = list(''.join(options).replace(node, ''))
        second += 1


if __name__ == '__main__':
    with open(INPUT) as input:
        tree, prereqs = buildTree(input)

        path = solve1(tree, prereqs)
        print(path)

        time = solve2(tree, prereqs)
        print(time)
