#!/usr/bin/env python3

def modify(start, change):
    operator = change[0]
    modifier = change[1:]

    if operator == '+':
        return start + int(modifier)
    if operator == '-':
        return start - int(modifier)
    return start

def solve1(input):
    acc = 0
    for line in input:
        acc = modify(acc, line.strip())
    print(acc)

def solve2(input):
    acc = 0
    freqs = {}
    while True:
        for line in input:
            acc = modify(acc, line.strip())
            if str(acc) in freqs:
                return acc
            freqs[str(acc)] = True
        input.seek(0)


if __name__ == '__main__':
    with open('input.txt') as input:
        print(solve1(input))
        print(solve2(input))
