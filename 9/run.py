#!/usr/bin/env python3
import time

from linkedlist import DoublyLinkedList

SAMPLE = False
if SAMPLE:
    INPUT = 'sample'
    PLAYER_NUM = 9
    LAST_MARBLE = 25
else:
    INPUT = 'input'
    PLAYER_NUM = 432
    LAST_MARBLE = 71019


def placeMarble(ring, marble, idx):
    if idx > len(ring):
        idx -= len(ring)
    ring.insert(idx, marble)
    return idx

def calculateHighScore(score):
    highScore = 0
    highPlayer = None
    for player, scores in score.items():
        score = sum(scores)
        if score > highScore:
            highScore = score
            highPlayer = player
    return highScore, highPlayer


def solve1(input):
    ring = [0]
    currentMarble = 0
    player = 1
    score = {}
    marble = 0

    for _ in range(LAST_MARBLE):
        marble += 1
        if marble % 23 == 0:
            if player not in score:
                score[player] = []
            currentMarble -= 7
            if currentMarble < 0:
                currentMarble = len(ring) + currentMarble
            score[player] += [marble, ring[currentMarble]]
            del ring[currentMarble]
        else:
            currentMarble += 2
            currentMarble = placeMarble(ring, marble, currentMarble)
        player += 1
        if player > PLAYER_NUM:
            player = 1
    highScore, _ = calculateHighScore(score)
    return highScore


def solve2(input):
    ring = DoublyLinkedList()
    ring.append(0)
    player = 1
    score = {}
    marble = 0
    last = LAST_MARBLE * 100

    node = ring.head
    for _ in range(last):
        marble += 1
        if marble % 23 == 0:
            if player not in score:
                score[player] = []

            for _ in range(7):
                if node.prev is not None:
                    node = node.prev
                else:
                    node = ring.getLast()
            score[player] += [marble, node.data]
            node = node.delete()
        else:
            if node.next is None:
                node = ring.head
            else:
                node = node.next
            node = node.appendAfter(marble)
        player += 1
        if player > PLAYER_NUM:
            player = 1
    highScore, _ = calculateHighScore(score)
    return highScore

if __name__ == '__main__':
    print(solve1(input))
    print(solve2(input))
