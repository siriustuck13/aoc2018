#!/usr/bin/env python3
import curses
import time

GOBLIN = 'G'
ELF = 'E'
WALL = '#'
FLOOR = '.'

ELF_POWER = 3

class Unit():
    id = 0

    def __init__(self, race, x, y):
        self.id = Unit.id
        self.race = race
        self.x = x
        self.y = y
        self.power = 3 if race == GOBLIN else ELF_POWER
        self.health = 200
        Unit.id += 1
    
    def __repr__(self):
        return self.race
    
    def attack(self, unit):
        unit.health -= self.power
        return unit.health <= 0

class Space():
    def __init__(self, char):
        self.char = char
        self.unit = None
    def __repr__(self):
        if self.unit is not None:
            return str(self.unit)
        return self.char
    
class Grid():
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.grid = makeBoard(width, height)
        self.units = []
        self.selected = None
    
    def render(self, win):
        for idx, row in enumerate(self.grid):
            units = []
            for cdx, space in enumerate(row):
                style = 0
                if space.unit is not None:
                    if space.unit == self.selected:
                        style = curses.A_BOLD
                    win.addstr(1+idx, 1+cdx, str(space), style)
                    units.append(space.unit)
                else:
                    win.addstr(1+idx, 1+cdx, str(space))
            units = ', '.join([(str(u.race) + '(' + str(u.health).rjust(3, ' ') + ')') for u in units]) + ' '*30
            win.addstr(1+idx, self.width+4, units)
    
    def at(self, x, y):
        return self.grid[y][x]
    
    def put(self, x, y, value):
        self.grid[y][x] = value
    
    def breadthFirstSearch(self, x, y, target):
        nodes = [Node(0, x, y)]
        nodeGrid = [[None for x in range(32)] for y in range(32)]
        prev = 0
        answer = None
        while len(nodes) > 0:
            n = nodes.pop(0)
            if n.g > prev:
                prev = n.g
                if answer is not None:
                    return answer.trace()
            for a in self.adjacentNodes(n.x, n.y, n, n.g+1):
                space = self.at(a.x, a.y)
                if space.unit is not None:
                    if space.unit.race == target:
                        if answer is not None:
                            assert answer.g == a.g
                            if readScore(a) < readScore(answer):
                                answer = a
                        else:
                            answer = a
                    else:
                        continue
                old = nodeGrid[a.y][a.x]
                if old is not None and old.g <= a.g:
                    continue
                nodeGrid[a.y][a.x] = a
                nodes.append(a)
    
    def adjacentNodes(self, x, y, parent=None, g=None):
        potential = [
            Node(g, x, y-1, parent), # up is best
            Node(g, x-1, y, parent), # left is second
            Node(g, x+1, y, parent), # right is third
            Node(g, x, y+1, parent), # down is worst
        ]
        nodes = []
        for n in potential:
            if self.at(n.x, n.y).char == WALL:
                continue
            nodes.append(n)
        assert len(nodes) <= 4
        return nodes
    
    def bestAttack(self, x, y, target):
        potential = [
            self.at(x, y-1), # up is best
            self.at(x-1, y), # left is second
            self.at(x+1, y), # right is third
            self.at(x, y+1), # down is worst
        ]
        best = None
        for s in potential:
            if s.unit is not None and s.unit.race == target:
                if best is None or s.unit.health < best.health:
                    best = s.unit
        return best

def printBFS(win, grid, nodeGrid):
    grid = [[str(grid.at(x, y)).rjust(2, ' ') for x in range(grid.width)] for y in range(grid.height)]
    for row in nodeGrid:
        for n in row:
            if n is not None:
                grid[n.y][n.x] = str(n.g).rjust(2, ' ')
    for idx, row in enumerate(grid):
        win.addstr(1+idx, 1, ''.join(row))
        win.refresh()

def pickBest(open, closed, next):
    for n in closed:
        if n.g > next.g:
            break
        if n.x == next.x and n.y == next.y:
            if n.g < next.g:
                return open
    insertNode(open, next)

class Node():
    def __init__(self, g, x, y, parent=None):
        self.g = g
        self.x = x
        self.y = y
        self.f = int(str(g) + str(y).rjust(2, '0') + str(x).rjust(2, '0'))
        self.parent = parent
    
    def __repr__(self):
        return '(' + str(self.x) + ',' + str(self.y) + ',' + str(self.g) + ')'
    
    def trace(self):
        if self.parent is None:
            return [self]
        return self.parent.trace() + [self]

def makeBoard(width, height):
    return [['@' for _ in range(width)] for _ in range(height)]

def insertNode(nodeList, node):
    if len(nodeList) == 0:
        nodeList.insert(0, node)
        return
    for idx, n in enumerate(nodeList):
        if n.x == node.x and n.y == node.y:
            break
        if n.g > node.g:
            nodeList.insert(idx, node)
            return
    nodeList.insert(len(nodeList), node)

def sortUnits(units):
    return sorted(units, key=lambda u: int(str(u.y).rjust(3, '0')+str(u.x).rjust(3, '0')))

def readScore(node):
    return int(str(node.y).rjust(3, '0')+str(node.x).rjust(3, '0'))
    
def parseInput(input, width, height=0):
    if height == 0:
        height = width
    grid = Grid(width, height)
    for y, line in enumerate(input):
        for x, char in enumerate(line.strip()):
            s = Space(char)
            if char in [GOBLIN, ELF]:
                u = Unit(char, x, y)
                s.char = '.'
                s.unit = u
                grid.units.append(u)
            grid.put(x, y, s)
    return grid

def takeTurn(grid, unit):
    if unit.health < 0:
        return
    grid.selected = unit
    opponent = ELF if unit.race == GOBLIN else GOBLIN
    #trace = grid.findClosest(unit.x, unit.y, opponent)
    trace = grid.breadthFirstSearch(unit.x, unit.y, opponent)
    if trace is None:
        # No movement found
        return
    if len(trace) > 2:
        # Move to trace[1] (first step)
        old = grid.at(unit.x, unit.y)
        n = trace[1]
        unit.x = n.x
        unit.y = n.y
        new = grid.at(unit.x, unit.y)
        new.unit = unit
        old.unit = None
        pass
    if len(trace) in [2,3]:
        # attack, or move then attack
        a = grid.bestAttack(unit.x, unit.y, opponent)
        killed = unit.attack(a)
        if killed:
            grid.at(a.x, a.y).unit = None
            grid.units.remove(a)
            return killed

def getWinner(units):
    winner = None
    for u in units:
        if winner is not None and u.race != winner:
            return None
        winner = u.race
    return winner

def solve(win, grid, debug=False):
    win.clear()
    round = 0
    while True:
        grid.units = sortUnits(grid.units)
        idx = 0
        while idx < len(grid.units):
            unit = grid.units[idx]
            killed = takeTurn(grid, unit)
            if killed:
                idx = grid.units.index(unit)
            if debug:
                win.addstr(0, 1, "Round: " + str(round) + " --- Elf Power: " + str(ELF_POWER))
                grid.render(win)
                win.refresh()
                time.sleep(0.001)

            if unit == grid.units[-1]:
                round += 1

            if getWinner(grid.units) is not None:
                return sum([u.health for u in grid.units]) * round
            idx += 1
            

def main(stdscr):
    global ELF_POWER
    curses.start_color()
    stdscr.clear()
    win = curses.newwin(100, 100, 1, 1)

    # Samples
    with open('choiceTest') as input:
        Unit.id = 1
        grid = parseInput(input, 7, 5)
        e1 = grid.units[0]
        step = grid.breadthFirstSearch(e1.x, e1.y, GOBLIN)[1]
        assert step.x == 3
        assert step.y == 1
    with open('sample') as input:
        Unit.id = 1
        grid = parseInput(input, 7)
        assert solve(win, grid) == 27730
    with open('test1') as input:
        Unit.id = 1
        grid = parseInput(input, 7)
        assert solve(win, grid) == 36334
    with open('test2') as input:
        Unit.id = 1
        grid = parseInput(input, 7)
        assert solve(win, grid) == 39514
    with open('test3') as input:
        Unit.id = 1
        grid = parseInput(input, 7)
        assert solve(win, grid) == 27755
    with open('test4') as input:
        Unit.id = 1
        grid = parseInput(input, 7)
        assert solve(win, grid) == 28944
    with open('test5') as input:
        Unit.id = 1
        grid = parseInput(input, 9)
        assert solve(win, grid) == 18740

    # Input
    with open('input') as input:
        Unit.id = 1
        grid = parseInput(input, 32)
        answer = solve(win, grid, True)
        time.sleep(5)
        win.clear()
        win.addstr(0, 0, "Q1. " + str(answer))
        win.refresh()
        time.sleep(5)
    # Part 2
    while True:
        try:
            with open('input') as input:
                Unit.id = 1
                grid = parseInput(input, 32)
                elves = len([e for e in grid.units if e.race == ELF])
                answer = solve(win, grid, True)
                remaining = len([e for e in grid.units if e.race == ELF])
                assert elves == remaining
                time.sleep(5)
                win.clear()
                win.addstr(0, 0, "Q2. " + str(answer))
                win.refresh()
                time.sleep(20)
                return
        except Exception:
            ELF_POWER += 1

if __name__ == '__main__':
    curses.wrapper(main)