#!/usr/bin/env python3
import os
import time

DEBUG = False

def turnLeft(char):
    if char =='>':
        return '^'
    if char =='^':
        return '<'
    if char =='<':
        return 'v'
    if char =='v':
        return '>'
def turnRight(char):
    if char == '>':
        return 'v'
    if char == 'v':
        return '<'
    if char == '<':
        return '^'
    if char == '^':
        return '>'

class Road():
    def __init__(self, char):
        self.char = char
        self.car = None
    def __repr__(self):
        if self.car is not None:
            return str(self.car)
        return self.char

class Car():
    id = 1
    def __init__(self, char, x, y):
        self.char = char
        self.x = x
        self.y = y
        self.intersections = 0
        self.id = Car.id
        Car.id += 1

    def __repr__(self):
        #return str(self.id)
        return self.char
    
    def decideTurn(self, x, y):
        state = self.intersections % 3
        if state == 0:
            self.char = turnLeft(self.char)
        elif state == 1:
            pass
        elif state == 2:
            self.char = turnRight(self.char)
        self.intersections += 1
        
    def next(self):
        self.x
        self.y
        if self.char == '>':
            self.x += 1
        elif self.char == '<':
            self.x -= 1
        elif self.char == '^':
            self.y -= 1
        elif self.char == 'v':
            self.y += 1
        return self.x, self.y
    
class Track():
    def __init__(self, dim):
        self.grid = [[None for x in range(dim)] for y in range(dim)]
        self.cars = []
    
    def __repr__(self):
        out = ''
        for row in self.grid:
            for point in row:
                out += (str(point) if point is not None else ' ')
            out += '\n'
        return out

    def addRoad(self, char, x, y):
        r = Road(char)
        self.grid[y][x] = r

    def addCar(self, char, x, y):
        c = Car(char, x, y)
        self.grid[y][x].car = c
        self.cars.append(c)

    def moveCars(self, remove=False):
        while True:
            self.cars = sortCars(self.cars)
            i = 0
            while i < len(self.cars):
                car = self.cars[i]
                self.print()
                if car not in self.cars:
                    continue # Protect against deletion
                last = self.grid[car.y][car.x]
                x, y = car.next()
                next = self.grid[y][x]
                if next.char == '\\':
                    if car.char == '^': # up
                        car.char = '<'
                    elif car.char == 'v': # down
                        car.char = '>'
                    elif car.char == '<': # left
                        car.char = '^'
                    elif car.char == '>': # right
                        car.char = 'v'
                elif next.char == '/':
                    if car.char == '^': # up
                        car.char = '>'
                    elif car.char == 'v': # down
                        car.char = '<'
                    elif car.char == '<': # left
                        car.char = 'v'
                    elif car.char == '>': # right
                        car.char = '^'
                elif next.char == '+': # INTERSECTION
                    car.decideTurn(x, y)
                last.car = None
                if next.car is not None:
                    if remove:
                        self.cars.remove(car)
                        i-=1
                        try:
                            listcheck = self.cars[i]
                        except:
                            print(i, self.cars)
                            exit()
                        self.cars.remove(next.car)
                        if i == len(self.cars) or self.cars[i] != listcheck:
                            i-=1
                        next.car = None
                    else:
                        return x, y
                else:
                    next.car = car
                i+=1
                if i >= len(self.cars):
                    break
            if len(self.cars) == 1:
                return self.cars[0].x, self.cars[0].y
            elif len(self.cars) == 0:
                return None

    def print(self):
        if DEBUG:
            os.system('clear')
            print(self)
            time.sleep(0.5)

def sortCars(cars):
    return sorted(cars, key=lambda c: int(str(c.y).rjust(3, '0')+str(c.x).rjust(3, '0')))

def parse(input, dim):
    track = Track(dim)
    for y, line in enumerate(input):
        for x, char in enumerate(line):
            if char in "<>^v":
                #car
                if char in "<>":
                    track.addRoad('-', x, y)
                if char in "^v":
                    track.addRoad('|', x, y)
                track.addCar(char, x, y)
            if char in "+|-/\\":
                #road
                track.addRoad(char, x, y)
    return track
        
def solve1(input, dim):
    Car.id = 1
    track = parse(input, dim)
    return track.moveCars()

def solve2(input, dim):
    Car.id = 1
    track = parse(input, dim)
    return track.moveCars(remove=True)

if __name__ == '__main__':
    print('Day 13')
    with open('sample') as input:
        print('Sample 1:', solve1(input, 15))
    with open('sample2') as input:
        print('Sample 2:', solve2(input, 15))
    with open('input') as input:
        print('Answer 1:', solve1(input, 151))
        input.seek(0)
        print('Answer 2:', solve2(input, 151))



