#!/usr/bin/env python3
import json
import curses
import time


class Ground():
    def __init__(self, pad=None, scroll=0, yPad=80, xPad=300):
        self.pad = pad
        self.xMin = 500
        self.xMax = 500
        self.yMin = 100
        self.yMax = 0
        self.current = 0
        self.scroll = scroll
        self.yPad = yPad-1
        self.xPad = xPad-1

    def render(self, style=0):
        if self.pad is None:
            return
        if self.current > self.scroll + 50:
            self.scroll += 1
        self.pad.addstr(self.yMax+5, 0, "WaterCount: " +
                        str(self.countWater()).rjust(3, ' '))
        self.pad.addstr(self.yMax+6, 0, "SettleCount: " +
                        str(self.countSettled()).rjust(3, ' '))
        # print x numbers
        for x in range(self.xMin, self.xMax):
            for idx, c in enumerate(str(x)):
                self.pad.addch(idx, x-self.xMin+4, str(c))
        # print y numbers
        for y in range(self.yMax+1):
            if y > self.scroll and y < self.scroll + self.yPad:
                self.pad.addstr(y+3, 0, str(y).rjust(4, ' '))
        # print grid
        for y, row in enumerate(self.grid):
            if y > self.scroll and y < self.scroll + self.yPad:
                if y == self.current:
                    s = curses.A_BOLD
                else:
                    s = style
                for x, char in enumerate(row):
                    self.pad.addch(y+3, x+4, char, s)
        self.pad.refresh(0, 0, 0, 0, 3, self.xPad)
        self.pad.refresh(self.scroll+3, 0, 3, 0, self.yPad, self.xPad)

    def countWater(self):
        count = 0
        for row in self.grid:
            for char in row:
                if char in '|~':
                    count += 1
        return count - self.yMin

    def countSettled(self):
        count = 0
        for row in self.grid:
            for char in row:
                if char in '~':
                    count += 1
        return count

    def loadClay(self, clay):
        """ yeah... this load code is garbage, but good enough """
        for c in clay:
            x = expand(c['x'])
            y = expand(c['y'])
            if int(x[0]) < self.xMin:
                self.xMin = x[0]-1
            if int(x[-1]) > self.xMax:
                self.xMax = x[-1]+1
            if int(y[0]) < self.yMin:
                self.yMin = y[0]
            if int(y[-1]) > self.yMax:
                self.yMax = y[-1]
        self.grid = [[' ' for x in range(self.xMin, self.xMax+1)]
                     for y in range(self.yMax+1)]
        for c in clay:
            x = expand(c['x'])
            y = expand(c['y'])
            for i in x:
                for j in y:
                    self.grid[j][i-self.xMin] = '#'

    def at(self, x, y):
        return self.grid[y][x]

    def dropable(self, x, y):
        return self.isOpen(x, y+1)

    def isOpen(self, x, y):
        return self.at(x, y) in ' |!'

    def fill(self, x, y):
        self.grid[y][x] = '|'
        while y < self.yMax:
            self.current = y
            if y > self.scroll:
                self.render()
            settled = False
            dy = 1
            x = 0
            while x < len(self.grid[y]):
                char = self.at(x, y)
                if char == '|':
                    if self.dropable(x, y):
                        # drop
                        self.grid[y+1][x] = '|'
                    else:
                        # spread
                        lDrop, lx = self.flow(x, y, -1)
                        rDrop, rx = self.flow(x, y, 1)
                        if not (lDrop or rDrop):
                            # no drop occurred - settle
                            for i in range(lx, rx+1):
                                self.grid[y][i] = '~'
                            settled = True
                x += 1
            # finalize
            for x, char in enumerate(self.grid[y]):
                if char == '!':
                    self.grid[y][x] = '|'
                    dy = 0
            if settled:
                dy = -1
            y += dy
        return self.countWater()

    def flow(self, x, y, dx):
        x += dx
        while self.isOpen(x, y):
            if self.at(x, y) == ' ':
                self.grid[y][x] = '!'
            if self.dropable(x, y):
                return True, x
            x += dx
        return False, x-dx


def parse(line):
    parts = line.split(',')
    p1 = parts[0].split('=')
    p2 = parts[1][1:].split('=')

    coords = {}
    coords[p1[0]] = p1[1]
    coords[p2[0]] = p2[1]
    return coords


def expand(coord):
    c = coord.split('..')
    c = [n for n in range(int(c[0]), int(c[-1])+1)]
    return c


def solve(ground, clay):
    ground.loadClay(clay)
    return ground.fill(500-ground.xMin, 0)


def main(stdscr):
    curses.start_color()
    pad = curses.newpad(2000, 2000)
    height, width = stdscr.getmaxyx()

    with open('sample') as input:
        clay = [parse(line.strip()) for line in input]
        g = Ground(pad, yPad=height, xPad=width)
        count = solve(g, clay)
        g.render(style=curses.A_BOLD)
        assert count == 57
        pad.getch()

    with open('input') as input:
        clay = [parse(line.strip()) for line in input]
        g = Ground(pad, yPad=height, xPad=width)
        count = solve(g, clay)
        g.render(style=curses.A_BOLD)
        pad.getch()


DEBUG = True
if __name__ == '__main__':
    if DEBUG:
        curses.wrapper(main)

    with open('sample') as input:
        clay = [parse(line.strip()) for line in input]
        g = Ground(None)
        count = solve(g, clay)
        print('Sample1:', count)
        assert count == 57
        print('Sample2:', g.countSettled())
        assert g.countSettled() == 29

    with open('input') as input:
        clay = [parse(line.strip()) for line in input]
        g = Ground(None)
        print('Q1:', solve(g, clay))
        print('Q2:', g.countSettled())
