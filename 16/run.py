#!/usr/bin/env python3
import json

METHODS = [
    "addr",
    "addi",
    "mulr",
    "muli",
    "banr",
    "bani",
    "borr",
    "bori",
    "setr",
    "seti",
    "gtir",
    "gtri",
    "gtrr",
    "eqir",
    "eqri",
    "eqrr",
]

class CPU():
    def __init__(self, register, instruction):
        self.register = register
        self.opcode = instruction[0]
        self.a = instruction[1]
        self.b = instruction[2]
        self.c = instruction[3]
        pass

    def copyRegister(self):
        return [ x for x in self.register ]
    
    def addr(self):
        """
        (add register) stores into register C the result of adding register A and register B.
        """
        r = self.copyRegister()
        r[self.c] = r[self.a] + r[self.b]
        return r
    def addi(self):
        """
        (add immediate) stores into register C the result of adding register A and value B.
        """
        r = self.copyRegister()
        r[self.c] = r[self.a] + self.b
        return r
    def mulr(self):
        """
        (multiply register) stores into register C the result of multiplying register A and register B.
        """
        r = self.copyRegister()
        r[self.c] = r[self.a] * r[self.b]
        return r
    def muli(self):
        """
        (multiply immediate) stores into register C the result of multiplying register A and value B.
        """
        r = self.copyRegister()
        r[self.c] = r[self.a] * self.b
        return r
    def banr(self):
        """
        (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
        """
        r = self.copyRegister()
        r[self.c] = r[self.a] & r[self.b]
        return r
    def bani(self):
        """
        (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
        """
        r = self.copyRegister()
        r[self.c] = r[self.a] & self.b
        return r
    def borr(self):
        """
        (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
        """
        r = self.copyRegister()
        r[self.c] = r[self.a] | r[self.b]
        return r
    def bori(self):
        """
        (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
        """
        r = self.copyRegister()
        r[self.c] = r[self.a] | self.b
        return r
    def setr(self):
        """
        (set register) copies the contents of register A into register C. (Input B is ignored.)
        """
        r = self.copyRegister()
        r[self.c] = r[self.a]
        return r
    def seti(self):
        """
        (set immediate) stores value A into register C. (Input B is ignored.)
        """
        r = self.copyRegister()
        r[self.c] = self.a
        return r
    def gtir(self):
        """
        (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
        """
        r = self.copyRegister()
        r[self.c] = 1 if self.a > r[self.b] else 0
        return r
    def gtri(self):
        """
        (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
        """
        r = self.copyRegister()
        r[self.c] = 1 if r[self.a] > self.b else 0
        return r
    def gtrr(self):
        """
        (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
        """
        r = self.copyRegister()
        r[self.c] = 1 if r[self.a] > r[self.b] else 0
        return r
    def eqir(self):
        """
        (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0.
        """
        r = self.copyRegister()
        r[self.c] = 1 if self.a == r[self.b] else 0
        return r
    def eqri(self):
        """
        (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
        """
        r = self.copyRegister()
        r[self.c] = 1 if r[self.a] == self.b else 0
        return r
    def eqrr(self):
        """
        (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
        """
        r = self.copyRegister()
        r[self.c] = 1 if r[self.a] == r[self.b] else 0
        return r

def compare(input):
    init = input.readline().strip()
    if init == '':
        return None, None
    inst = input.readline().strip()
    final = input.readline().strip()

    init = json.loads(init[8:])
    inst = [int(i) for i in inst.split(' ')]
    final = json.loads(final[8:])

    cpu = CPU(init, inst)
    ok = []
    for m in METHODS:
        try:
            result = getattr(cpu, m)()
            if final == result:
                ok.append(m)
        except:
            pass
    return cpu.opcode, ok
        
def andLists(base, newItems):
    andList = []
    for i in base:
        if i in newItems:
            andList.append(i)
    return andList

def solveRegister(register):
    methodMap = {}
    for m in METHODS:
        methodMap[m] = None

    solved = False
    while not solved:
        for opcode, methods in register.items():
            if len(methods) == 1:
                methodMap[methods[0]] = opcode
            register[opcode] = [m for m in methods if methodMap[m] is None]
        
        solved = True
        for m in METHODS:
            if methodMap[m] is None:
                solved = False
    
    solution = {}
    for m in METHODS:
        solution[methodMap[m]] = m
    return solution

if __name__ == '__main__':
    cpu = CPU([4,3,2,1], [None, 0, 1, 2])
    assert cpu.addr() == [4,3,7,1]
    cpu = CPU([1,2,3,4], [None, 0, 7, 3])
    assert cpu.addi() == [1,2,3,8]

    with open('sample') as input:
        opcode, possible = compare(input)
        assert opcode == 9
        assert possible == [m for m in METHODS if m in ['addi', 'mulr', 'seti']]

    assert andLists([1,2,3,4], [3,4,5,6]) == [3,4]

    with open('input') as input:
        cases = True
        register = {}
        count = 0
        while True:
            opcode, possible = compare(input)
            if possible is None:
                break
            if len(possible) >= 3:
                count += 1
            if opcode not in register:
                register[opcode] = possible
            register[opcode] = andLists(register[opcode], possible)
            input.readline()
        print('Q1.', count)

        codeMap = solveRegister(register)
        register = [0, 0, 0, 0]
        for line in input:
            if line.strip() == '':
                continue
            inst = [int(i) for i in line.strip().split(' ')]
            cpu = CPU(register, inst)
            register = getattr(cpu, codeMap[cpu.opcode])()
        print('Q2.', register[0])

            

