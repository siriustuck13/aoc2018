#!/usr/bin/env python3
import time
import curses
import os

OPEN = '.'
TREE = '|'
LUMBER = '#'

class Grid():
    def __init__(self, dim, input, win):
        self.dim = dim
        self.grid = makeGrid(dim)
        self.load(input)
        self.win = win
        pass
    
    def __repr__(self):
        out = ''
        for row in self.grid:
            for char in row:
                out += char
            out += '\n'
        return out

    def render(self, iter, tCount, lCount):
        self.win.erase()
        for idx, row in enumerate(self.grid):
            self.win.addstr(idx, 1, ''.join(row))

        self.win.addstr(self.dim + 2, 1, '        Time: ' + str(iter).rjust(6))
        self.win.addstr(self.dim + 3, 1, '  Tree Count: ' + str(tCount).rjust(6))
        self.win.addstr(self.dim + 4, 1, 'Lumber Count: ' + str(lCount).rjust(6))
        self.win.addstr(self.dim + 5, 1, '      Answer: ' + str(tCount*lCount).rjust(6))
        self.win.refresh()
        time.sleep(0.01)

    def at(self, x, y):
        if x < 0 or x >= self.dim or y < 0 or y >= self.dim:
            return None
        return self.grid[y][x]
    
    def tick(self):
        newGrid = makeGrid(self.dim)
        for y, line in enumerate(self.grid):
            for x, char in enumerate(line):
                place = char
                tCount = self.countAdjacent(x, y, TREE)
                lCount = self.countAdjacent(x, y, LUMBER)
                if char == OPEN:
                    if tCount >= 3:
                        place = TREE
                elif char == TREE:
                    if lCount >= 3:
                        place = LUMBER
                elif char == LUMBER:
                    if (lCount == 0) or (tCount == 0):
                        place = OPEN
                newGrid[y][x] = place
        self.grid = newGrid

    def countAdjacent(self, x, y, char):
        ul = self.at(x-1,y-1)
        um = self.at(x,y-1)
        ur = self.at(x+1,y-1)
        ml = self.at(x-1,y)
        mr = self.at(x+1,y)
        dl = self.at(x-1,y+1)
        dm = self.at(x,y+1)
        dr = self.at(x+1,y+1)
        return [ul,um,ur,ml,mr,dl,dm,dr].count(char)
    
    def count(self, char):
        c = 0
        for line in self.grid:
            c += line.count(char)
        return c
    
    def load(self, input):
        for y, line in enumerate(input):
            for x, char in enumerate(line.strip()):
                self.grid[y][x] = char
    
    def animate(self, duration):
        count = 0
        while True:
            tCount = self.count(TREE)
            lCount = self.count(LUMBER)
            self.render(count, tCount, lCount)
            #if (count >= 440) and (count - 440) % 28 == 0:
            #    self.win.getch()
            count += 1
            if count > duration:
                return
            self.tick()


    
def makeGrid(dim):
    return [['@' for _ in range(dim)] for _ in range(dim)]

def parse(line):
    pass

def solve(input):
    for line in input:
        parse(line)
    return False

def main(stdscr):
    curses.noecho()
    curses.cbreak()
    win = curses.newwin(80, 80, 1, 1)
    win.idcok(False)
    win.idlok(False)

    with open('sample') as file:
        g = Grid(10, file, win)
        g.animate(10)
        tCount = g.count(TREE)
        lCount = g.count(LUMBER)
        assert tCount == 37
        assert lCount == 31
        assert tCount * lCount == 1147
    
    with open('input') as file:
        g = Grid(50, file, win)
        g.animate(10)
        tCount = g.count(TREE)
        lCount = g.count(LUMBER)
    
    with open('input') as file:
        g = Grid(50, file, win)
        g.animate(1000000000)
        tCount = g.count(TREE)
        lCount = g.count(LUMBER)

if __name__ == '__main__':
    curses.wrapper(main)