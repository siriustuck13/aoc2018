#!/usr/bin/env python3
import time

SAMPLE = False
if SAMPLE:
    INPUT = 'sample'
    MODIFIER = 0
    ELFCOUNT = 2
    FOUND = 'ABCDEF'
else:
    INPUT = 'input'
    MODIFIER = 60
    ELFCOUNT = 5
    FOUND = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

class Node:
    def __init__(self, metadata, children):
        self.metadata = metadata
        self.children = children
    
    def print(self):
        print(self.metadata)
        for child in self.children:
            child.print()
    
    def sum(self):
        total = 0
        total += sum(int(m) for m in self.metadata)
        total += sum(c.sum() for c in self.children)
        return total
    
    def sumChildren(self):
        total = 0
        if len(self.children) == 0:
            total += sum(int(m) for m in self.metadata)
        else:
            for m in self.metadata:
                i = int(m) - 1
                if i < len(self.children):
                    total += self.children[i].sumChildren()
        return total

def parseNode(input, idx=0):
    childCount = int(input[idx])
    metaCount = int(input[idx+1])
    idx += 2
    children = []
    for _ in range(childCount):
        child, idx = parseNode(input, idx)
        children.append(child)
    metadata = []
    for _ in range(metaCount):
        meta = input[idx]
        idx += 1
        metadata.append(meta)
    return Node(metadata, children), idx


if __name__ == '__main__':
    with open(INPUT) as input:
        line = input.readline()
        node, _ = parseNode(line.split(' '))

        print(node.sum())
        print(node.sumChildren())
