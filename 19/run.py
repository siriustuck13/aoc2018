#!/usr/bin/env python3

import time
import curses
from cpu import CPU

from math import sqrt


def solve(puzzle, register, ip, debug=False):
    instructions = []
    for line in puzzle:
        opr = line[0:4]
        instr = [int(i) for i in line[5:].split(' ')]
        instructions.append([opr] + instr)

    i = 0
    cpu = CPU(register)
    while i < len(instructions):
        cpu.register[ip] = i
        cpu.execute(instructions[i])
        i = cpu.register[ip]
        i += 1
    return cpu.register[0]

def solve2(puzzle, register, ip, debug=False):
    instructions = []
    for line in puzzle:
        opr = line[0:4]
        instr = [int(i) for i in line[5:].split(' ')]
        instructions.append([opr] + instr)

    i = 0
    cpu = CPU(register)
    while i < len(instructions):
        cpu.register[ip] = i
        cpu.execute(instructions[i])
        i = cpu.register[ip]
        if i == 0: # get divisors of value stored at this point
            break
        i += 1
    
    return int(sum(divisors(cpu.register[1])))

def divisors(t):
    s=round(sqrt(t))
    list1=[]
    for i in range(1,s+1):
        if t%i==0:
            list1.append(i)
            list1.append(t/i)
    return list1


if __name__ == '__main__':
    with open('sample') as file:
        answer = solve(file, [0,0,0,0,0,0], 0)
        print('Sample.', answer)
        assert answer == 6
    
    with open('input') as file:
        answer = solve(file, [0,0,0,0,0,0], 5)
        print('Q1.', str(answer))

    with open('input') as file:
        answer = solve2(file, [0,0,0,0,0,0], 5)
        assert answer == 1836
    
    with open('input') as file:
        answer = solve2(file, [1,0,0,0,0,0], 5)
        print('Q2.', str(answer))
