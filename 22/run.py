#!/usr/bin/env python3
import curses
import time

ROCKY = '.'
WET = '='
NARROW = '|'

GEAR = 'gear'
TORCH = 'torch'

YMAX = 0
XMAX = 0

class Region():
    def __init__(self, x, y, index, depth):
        self.x = x
        self.y = y
        self.index = index
        self.erosion = (depth + index) % 20183
        self.type = self.getType()
    
    def __repr__(self):
        return str(self.x) + ', ' + str(self.y) + '----' + str(self.type)

    def getType(self):
        return terrain(self.erosion)

class Cave():
    def __init__(self, depth=510, y=10, width=10, pad=None):
        self.map = {}
        self.depth = depth
        self.y = y
        self.width = width
        self.pad = pad

    def generate(self):
        for y in range(0, self.y+1):
            for x in range(0, self.width+50):
                self.map[(x, y)] = Region(x, y, self.indexAt(x, y), self.depth)
    
    def print(self, x=None, y=None, gear=None, g=0):
        if self.pad is None:
            return
            #for coord in self.map:
                #region = self.map[coord]
                #print(self.map[coord])
        for coord in self.map:
            region = self.map[coord]
            if self.width == coord[0] and self.y == coord[1]:
                self.pad.addstr(coord[1], coord[0], 'T', curses.A_BOLD)
            elif x == coord[0] and y == coord[1]:
                self.pad.addstr(y, x, 'X', curses.A_BOLD)
            else:
                self.pad.addstr(coord[1], coord[0], region.type)
        self.pad.addstr(self.y + 3, 0, 'Tool: ' + str(gear).ljust(4))
        self.pad.addstr(self.y + 4, 0, 'Gval: ' + str(g))
        self.pad.refresh(0, 0, 0, 0, YMAX-1, XMAX-1)

    def indexAt(self, x, y):
        if (x, y) in self.map:
            return self.map[(x,y)].index
        if y == self.y and x == self.width:
            i = 0
        elif y == 0:
            i = x * 16807
        elif x == 0:
            i = y * 48271
        else:
            i = self.erosionAt(x-1, y) * self.erosionAt(x, y-1)
        self.map[(x, y)] = Region(x, y, i, self.depth)
        return i
    
    def erosionAt(self, x, y):
        if (x, y) in self.map:
            return self.map[(x,y)].erosion
        return (self.depth + self.indexAt(x, y)) % 20183

    
    def terrainAt(self, x, y):
        if (x, y) in self.map:
            return self.map[(x,y)].type
        return terrain(self.erosionAt(x, y))
    
    def assessRisk(self):
        risk = 0
        for y in range(self.y+1):
            for x in range(self.width+1):
                t = self.map[(x,y)].type
                if t == WET:
                    risk += 1
                if t == NARROW:
                    risk += 2
        return risk
    
    def findBest(self):
        nodes = [Node(0, 0, 0)]
        nodeGrid = [[None for x in range(self.width+1000)] for y in range(self.depth+1000)]
        yMax = 0
        xMax = 0
        while len(nodes) > 0:
            nodes = sortByG(nodes)
            n = nodes.pop(0)
            #time.sleep(0.1)
            self.print(n.x, n.y)
            if n.y > yMax:
                yMax = n.y
            if n.x > xMax:
                xMax = n.x
            if n.x == self.width and n.y == self.y:
                for t in n.trace():
                    if self.pad is not None:
                        self.print(t.x, t.y, t.tool, t.g)
                        self.pad.getch()
                print(xMax, yMax)
                return n.g + (0 if n.tool == TORCH else 7)
            for a in self.adjacentNodes(n.x, n.y, n, n.g):
                #if a.x == self.width and a.y == self.y:
                #    return a.g + (1 if a.tool == GEAR else 0)
                old = nodeGrid[a.y][a.x]
                if old is not None and old.g <= a.g:
                    continue
                nodeGrid[a.y][a.x] = a
                nodes.append(a)
    
    def adjacentNodes(self, x, y, parent=None, g=0):
        fromTerrain = self.terrainAt(x, y)
        potential = [
            (x, y-1),
            (x, y+1),
            (x-1, y),
            (x+1, y)
        ]
        nodes = []
        for c in potential:
            x = c[0]
            y = c[1]
            if y < 0 or x < 0:
                continue
            toTerrain = self.terrainAt(x, y)
            if fromTerrain == toTerrain:
                score = g+1
                tool = parent.tool
            elif fromTerrain == ROCKY:
                if toTerrain == WET:
                    score = g + (8 if parent.tool == TORCH else 1)
                    tool = GEAR
                elif toTerrain == NARROW:
                    score = g + (8 if parent.tool == GEAR else 1)
                    tool = TORCH
            elif fromTerrain == WET:
                if toTerrain == ROCKY:
                    score = g + (8 if parent.tool is None else 1)
                    tool = GEAR
                elif toTerrain == NARROW:
                    score = g + (8 if parent.tool == GEAR else 1)
                    tool = None
            elif fromTerrain == NARROW:
                if toTerrain == ROCKY:
                    score = g + (8 if parent.tool is None else 1)
                    tool = GEAR
                elif toTerrain == WET:
                    score = g + (8 if parent.tool == TORCH else 1)
                    tool = None
            nodes.append(Node(score, x, y, parent, tool))
        assert len(nodes) <= 4
        return nodes


class Node():
    def __init__(self, g, x, y, parent=None, tool=TORCH):
        self.g = g
        self.x = x
        self.y = y
        self.f = int(str(g) + str(y).rjust(2, '0') + str(x).rjust(2, '0'))
        self.tool = tool
        self.parent = parent
    
    def __repr__(self):
        return '(' + str(self.x) + ',' + str(self.y) + ',' + str(self.g) + ')  ' + str(self.tool)
    
    def trace(self):
        if self.parent is None:
            return [self]
        return self.parent.trace() + [self]
    
def sortByManhatten(nodes, x, y):
    return sorted(nodes, key=lambda n: (n.x-x + n.y-y))

def sortByG(nodes):
    return sorted(nodes, key=lambda n: n.g)

def terrain(n):
    m = n % 3
    if m == 0:
        return ROCKY
    elif m == 1:
        return WET
    elif m == 2:
        return NARROW

def solve(depth=510, x=10, y=10, pad=None):
    cave = Cave(depth=depth, y=y, width=x, pad=pad)
    cave.generate()
    cave.print()
    risk = cave.assessRisk()
    best = cave.findBest()
    if pad is not None:
        pad.getch()
    return risk, best

def main(stdscr):
    global YMAX
    global XMAX

    pad = curses.newpad(2000, 2000)
    YMAX, XMAX = stdscr.getmaxyx()
    solve(pad=pad)
    pad.clear()
    solve(depth=11394, x=7, y=701, pad=pad)


DEBUG = False
if __name__ == '__main__':
    cave = Cave()
    assert cave.indexAt(0,0) == 0
    assert cave.erosionAt(0,0) == 510
    assert cave.indexAt(1,0) == 16807
    assert cave.erosionAt(1,0) == 17317
    assert cave.indexAt(0,1) == 48271
    assert cave.erosionAt(0,1) == 8415
    assert cave.indexAt(1,1) == 145722555
    assert cave.erosionAt(1,1) == 1805
    assert cave.indexAt(10, 10) == 0
    assert cave.erosionAt(10, 10) == 510
    q1, q2 = solve(510, 10, 10)
    assert q1 == 114
    assert q2 == 45

    if DEBUG:
        curses.wrapper(main)
    
    risk, best = solve(depth=11394, x=7, y=701)
    print('Q1.', risk)
    print('Q2.', best)
